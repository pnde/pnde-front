import useUndoable from "use-undoable";
import { addEdge, applyEdgeChanges, applyNodeChanges, useEdgesState, useNodesState } from "reactflow";
import { MemoryRouter as Router } from "react-router";
import { Route, Routes } from "react-router-dom";
import MainPage from "./Pages/MainPage";
import PlacePage from "./Pages/PlacePage";
import TransitionPage from "./Pages/TransitionPage";
import { createContext, useCallback, useState } from "react";
import CodeEditorWindow from "./Pages/CodeEditor";
import { SPNPOptionsPage } from "./Pages/SPNPOptionsPage";
import { defaultOptions } from "./Engines/spnp/Options";
import SourcePage from "./Pages/SourcePage";
import { SourceTransitionPage } from "./Pages/SourceTransitionPage";
import { LocationTransitionPage } from "./Pages/LocationTransitionPage";
import { LoginPage } from "./Pages/LoginPage";
import { SelectModelPage } from "./Pages/SelectModelPage";
import { CreateModelPage } from "./Pages/CreateModelPage";
import { RestManager } from "./AppStateManager";
import { EdgePage } from "./Pages/EdgePage";
import { SPNPResultsPage } from "./Pages/SPNPResultsPage";
import LocationPage from "./Pages/LocationPage";

export const SelectedNodeContext = createContext({})

export function App() {
    const initialState = {
        nodes: [],
        edges: [],
    }

    const [elements, setElements, { undo, redo }] = useUndoable(initialState)
    const [options, setOptions] = useState(JSON.parse(localStorage.getItem("options")) || defaultOptions)
    console.log(options)

    if (localStorage.getItem('codeTarget') === 'userCode') {
        const code = localStorage.getItem('code');
        RestManager.saveModel(() => {
        }, 'extra_code', code)
        localStorage.setItem("user_code", code)
        localStorage.removeItem('code')
        localStorage.removeItem('codeTarget')
        localStorage.removeItem('resultPage')
    }

    // useEffect(() => {
    //     console.log(elements);
    // }, [elements]);

    const triggerUpdate = useCallback(
        (t, v) => {
            // To prevent a mismatch of state updates,
            // we'll use the value passed into this
            // function instead of the state directly.
            setElements(e => ({
                nodes: t === 'nodes' ? v : e.nodes,
                edges: t === 'edges' ? v : e.edges,
            }));
        },
        [setElements]
    );


    // We declare these callbacks as React Flow suggests,
    // but we don't set the state directly. Instead, we pass
    // it to the triggerUpdate function so that it alone can
    // handle the state updates.

    const onNodesChange = useCallback(
        (changes) => {
            if (changes && changes[0]['type'] === 'position') {
                setElements({
                    'edges': elements.edges,
                    nodes: applyNodeChanges(changes, elements.nodes)
                }, undefined, true)
            } else
                triggerUpdate('nodes', applyNodeChanges(changes, elements.nodes));
        },
        [triggerUpdate, elements.nodes]
    );

    const onEdgesChange = useCallback(
        (changes) => {
            triggerUpdate('edges', applyEdgeChanges(changes, elements.edges));
        },
        [triggerUpdate, elements.edges]
    );

    const onConnect = useCallback(
        (connection) => {
            triggerUpdate('edges', addEdge(connection, elements.edges));
        },
        [triggerUpdate, elements.edges]
    );
    const [rfInstance, setRfInstance] = useState(null);

    const [selectedNode, setSelectedNode] = useState({})
    const [selectedEdge, setSelectedEdge] = useState({})

    return (
        <SelectedNodeContext.Provider
            value={{ node: selectedNode, triggerUpdate: triggerUpdate, elements: elements }}>
            <Router>
                <Routes>
                    <Route path="/" element={<LoginPage />} key={1} />
                    <Route
                        path="/code"
                        element={<CodeEditorWindow
                            elements={elements}
                            node={selectedNode}
                        />}
                        key={1}
                    />
                    <Route
                        path="/main"
                        element={<MainPage
                            elements={elements}
                            onNodesChange={onNodesChange}
                            onEdgesChange={onEdgesChange}
                            onConnect={onConnect}
                            setRfInstance={setRfInstance}
                            setElements={setElements}
                            undo={undo}
                            redo={redo}
                            setSelectedNode={setSelectedNode}
                            rfInstance={rfInstance}
                            triggerUpdate={triggerUpdate}
                            selectedEdge={selectedEdge}
                            setSelectedEdge={setSelectedEdge}
                        />}
                        key={2}

                    />
                    <Route
                        path="/place"
                        element={<PlacePage
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                        />}
                    />
                    <Route
                        path="/sbpn/source"
                        element={<SourcePage
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                        />}
                    />
                    <Route
                        path="/transition/imm"
                        element={<TransitionPage
                            setSelectedNode={setSelectedNode}
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                            path="/transition/imm"
                        />}
                    />
                    <Route
                        path="/transition/timed"
                        element={<TransitionPage
                            setSelectedNode={setSelectedNode}
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                            path="/transition/timed"
                        />}
                    />
                    <Route
                        path="/sbpn/transition/imm"
                        element={<SourceTransitionPage
                            setSelectedNode={setSelectedNode}
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                            path="/sbpn/transition/imm"
                        />}
                    />
                    <Route
                        path="/sbpn/transition/timed"
                        element={<SourceTransitionPage
                            setSelectedNode={setSelectedNode}
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                            path="/sbpn/transition/timed"
                        />}
                    />
                    <Route path="/spnp/options"
                        element={<SPNPOptionsPage options={options} saveOptions={setOptions} />}
                    />
                    <Route path="/model/select"
                        element={<SelectModelPage setElements={setElements} />}
                    />
                    <Route path="/model/new"
                        element={<CreateModelPage />}
                    />
                    <Route path="/edge"
                        element={<EdgePage
                            selectedEdge={selectedEdge}
                            setSelectedEdge={setSelectedEdge}
                            elements={elements}
                            triggerUpdate={triggerUpdate} />}
                    />
                    <Route path="/spnp/run"
                        element={<SPNPResultsPage />}
                    />
                    <Route
                        path="/msrn/location"
                        element={<LocationPage
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                        />}
                    />
                    <Route
                        path="/msrn/transition/imm"
                        element={<LocationTransitionPage
                            setSelectedNode={setSelectedNode}
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                            path="/msrn/transition/imm"
                        />}
                    />
                    <Route
                        path="/msrn/transition/timed"
                        element={<LocationTransitionPage
                            setSelectedNode={setSelectedNode}
                            selectedNode={selectedNode}
                            triggerUpdate={triggerUpdate}
                            elements={elements}
                            path="/msrn/transition/timed"
                        />}
                    />
                </Routes>
            </Router>
        </SelectedNodeContext.Provider>
    )
}