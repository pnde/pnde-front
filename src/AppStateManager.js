import { EngineTypeKey, MSRNModelTypeKey, MTSRNModelTypeKey, ModelTypeKey, SBPNModelTypeKey } from "./Petri/Consts";
import { BackendHost } from "./Configs";
import { defaultOptions } from "./Engines/spnp/Options";

export const DefaultManager = {
    'NodeKey': 'SavedNode',
    'initApp': () => {
        localStorage.clear()
    },
    'setEngine': (engineName) => {
        localStorage.setItem(EngineTypeKey, engineName)
    },
    'setModelType': (modeType) => {
        localStorage.setItem(ModelTypeKey, modeType)
    },
    'getModelType': () => localStorage.getItem(ModelTypeKey),
    'isSBPNEnable': () => DefaultManager.getModelType() == SBPNModelTypeKey,
    'isMSRNEnable': () => DefaultManager.getModelType() == MSRNModelTypeKey,
    'isMTSRNEnable': () => DefaultManager.getModelType() == MTSRNModelTypeKey,
    'persistNode': (node) => localStorage.setItem(DefaultManager.NodeKey, node),
    'getPersistedNode': () => localStorage.getItem(DefaultManager.NodeKey),
}

export const RestManager = {
    getUrl: (path) => `${BackendHost}${path}`,
    getHeaders: () => ({
        'Content-type': 'application/json; charset=UTF-8',
        'Authorization': `Token ${localStorage.getItem("token")}`
    }),
    login: (username, password, rememberMe, resultCallback) => {
        fetch(RestManager.getUrl('/auth-token/'), {
            method: "POST",
            body: JSON.stringify({
                username: username,
                password: password
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }).then(response => response.json()).then((data) => {
            console.log(data)
            localStorage.setItem('token', data.token)
            if (rememberMe) {
                localStorage.setItem('username', username)
                localStorage.setItem('password', password)
                localStorage.setItem('rememberMe', rememberMe)
            }
            resultCallback(data)
        }).catch((err) => resultCallback({ err: err }))
    },
    createNewModel: (name, modelType, engine, resultCallBack) => {
        fetch(RestManager.getUrl('/petri/'), {
            method: "POST",
            body: JSON.stringify({
                name: name,
                model_type: modelType,
                engine: engine,
                options: engine == 20 ? defaultOptions : {}
            }),
            headers: RestManager.getHeaders()
        }).then(response => response.json()).then((data) => {
            localStorage.setItem('model_id', data.id)
            localStorage.setItem('model_name', data.name)
            localStorage.setItem("user_code", data.extra_code)
            DefaultManager.setModelType(modelType)
            DefaultManager.setEngine(engine)
            resultCallBack(data)
        }).catch((err) => console.log(err))
    },
    getModels: (resultCallBack) => {
        fetch(RestManager.getUrl('/petri/'), {
            headers: RestManager.getHeaders()
        }).then(response => response.json()).then(data => {
            resultCallBack(data)
        }).catch(err => console.log(err))
    },
    saveModel: (resultCallBack, field, value) => {
        fetch(RestManager.getUrl(`/petri/${localStorage.getItem('model_id')}/`), {
            headers: RestManager.getHeaders(),
            method: "PATCH",
            body: JSON.stringify({
                [field]: value
            })
        }).then((response) => response.json()).then(
            response => resultCallBack(response)
        ).catch(err => resultCallBack(err))
    },
    getSPNPCode: (resultCallBack) => {
        fetch(RestManager.getUrl(`/petri/${localStorage.getItem('model_id')}/spnp_code/`), {
            headers: RestManager.getHeaders()
        }).then(res => res.json()).then(code => {
            console.log(code)
            resultCallBack(code.code)
        })
    },
    runSPNP: (resultCallBack) => {
        fetch(RestManager.getUrl(`/petri/${localStorage.getItem('model_id')}/run_spnp/`), {
            headers: RestManager.getHeaders()
        }).then(res => res.json()).then(result => {
            console.log(result)
            resultCallBack(result)
        })
    },
    getModel: (resultCallBack) => {
        fetch(RestManager.getUrl(`/petri/${localStorage.getItem('model_id')}/`), {
            headers: RestManager.getHeaders(),
            method: "GET",
        }).then((response) => response.json()).then(
            response => resultCallBack(response)
        ).catch(err => resultCallBack(err))
    }
}