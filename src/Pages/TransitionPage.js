import { useNavigate } from "react-router";
import { Container, Form } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css'
import { useState } from "react";
import { getUpdatedNode, getUpdatedNodes } from "../Petri/utils";

const options = [
    { key: 1, text: 'Use Hard Coded Rate Value', value: 'use_hard_code' },
    { key: 2, text: 'Use Provided Function', value: 'use_provided_function' },
    { key: 3, text: 'Dependent on place', value: 'use_provided_function' },
]

const guardOption = [
    { key: 1, text: 'No Guard Function', value: 'no_guard' },
    { key: 2, text: 'Enable Guard Function', value: 'have_guard' },
]

const policyOptions = [
    { key: 1, text: 'PRI', value: 'PRI' },
    { key: 2, text: 'PRD', value: 'PRD' },
    { key: 3, text: 'PRR', value: 'PRR' },
]

export default function TransitionPage(props) {
    const navigate = useNavigate();
    const initialState = {
        name: props.selectedNode.name,
        rate: props.selectedNode.rate,
        rate_mode: props.selectedNode.rate_mode,
        rate_function_code: props.selectedNode.rate_function_code,
        priority: props.selectedNode.priority,
        policy: props.selectedNode.policy,
        guard_mode: props.selectedNode.guard_mode,
        guard_function: props.selectedNode.guard_function,
        place_name: props.selectedNode.place_name
    }
    const [state, setState] = useState(initialState)

    const handleChange = (e, { name, value }) => {
        setState({ ...state, [name]: value })
    }

    if (localStorage.getItem('codeTarget')) {
        setState({ ...state, [localStorage.getItem('codeTarget')]: localStorage.getItem('code') })
        localStorage.removeItem('code')
        localStorage.removeItem('codeTarget')
        localStorage.removeItem('resultPage')
    }

    return (
        <Container style={{ margin: 20 }}>
            <Form onSubmit={() => {
                let childrenState = {}
                if (props.getChildrenState !== undefined) childrenState = props.getChildrenState()
                console.log(childrenState)
                const updatedNodes = getUpdatedNodes(
                    props.selectedNode.id,
                    { ...state, ...childrenState, data: { label: state.name } },
                    props.elements.nodes
                );
                console.log(updatedNodes)
                props.triggerUpdate('nodes', updatedNodes)
                navigate("/main")
            }}>
                <Form.Field>
                    <label>Transition ID</label>
                    <Form.Input placeholder='Transition ID'
                        value={props.selectedNode.id}
                        disabled
                    />
                </Form.Field>
                <Form.Field>
                    <label>Transition Name</label>
                    <Form.Input placeholder='Transition Name'
                        value={state.name}
                        name='name'
                        onChange={handleChange}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Rate</label>
                    <Form.Input placeholder='Rate'
                        type='number'
                        name='rate'
                        min={0.0}
                        step="any"
                        value={state.rate}
                        onChange={handleChange}
                        required
                    />
                </Form.Field>
                <Form.Field>
                    <label>Priority</label>
                    <Form.Input placeholder='Priority'
                        type='number'
                        name='priority'
                        min={0}
                        value={state.priority}
                        onChange={handleChange}
                        required
                    />
                </Form.Field>
                <Form.Group widths={"equal"}>
                    <Form.Select
                        fluid
                        label='Policy'
                        options={policyOptions}
                        name='policy'
                        onChange={handleChange}
                        value={state.policy}
                    />
                </Form.Group>
                <Form.Group widths={"equal"}>
                    <Form.Select
                        fluid
                        label='Rate Mode'
                        options={options}
                        name='rate_mode'
                        onChange={handleChange}
                        value={state.rate_mode}
                    />
                </Form.Group>
                <Form.Button
                    fluid
                    disabled={state.rate_mode === "use_hard_code"}
                    onClick={() => {
                        props.setSelectedNode(getUpdatedNode(props.selectedNode.id, state, props.elements.nodes))
                        localStorage.setItem('codeTarget', 'rate_function_code')
                        localStorage.setItem('code', state.rate_function_code)
                        localStorage.setItem('resultPage', props.path)
                        navigate("/code")
                    }}
                >
                    Edit Rate Code
                </Form.Button>

                <Form.Group widths={"equal"}>
                    <Form.Select
                        fluid
                        label='Guard Mode'
                        options={guardOption}
                        name='guard_mode'
                        onChange={handleChange}
                        value={state.guard_mode}
                    />
                </Form.Group>
                <Form.Button
                    fluid
                    disabled={state.guard_mode === "no_guard"}
                    onClick={() => {
                        props.setSelectedNode(getUpdatedNode(props.selectedNode.id, state, props.elements.nodes))
                        localStorage.setItem('codeTarget', 'guard_function')
                        localStorage.setItem('code', state.guard_function)
                        localStorage.setItem('resultPage', props.path)
                        navigate("/code")
                    }}
                >
                    Edit Guard Function Code
                </Form.Button>

                {props.children}

                <Form.Button primary fluid>
                    Save
                </Form.Button>
                <Form.Button
                    fluid
                    onClick={(event) => {
                        event.preventDefault()
                        setState(initialState)
                    }}>
                    Reset
                </Form.Button>
                <Form.Button
                    negative
                    fluid
                    onClick={() => navigate("/main")}>
                    Cancel
                </Form.Button>
            </Form>
        </Container>
    )
}