import TransitionPage from "./TransitionPage";
import {useState} from "react";
import {mergeSourceRates} from "../Petri/SourceBased/Utils";
import {Form, Table} from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css'

export function SourceTransitionPage(props) {
    const initialSourceRates = mergeSourceRates(props.elements.nodes, props.selectedNode.source_rates);
    console.log(initialSourceRates)
    const [sourceRates, setSourceRates] = useState(
        initialSourceRates
    )
    console.log(sourceRates)
    return <TransitionPage setSelectedNode={props.setSelectedNode}
                           selectedNode={props.selectedNode}
                           triggerUpdate={props.triggerUpdate}
                           elements={props.elements}
                           path={props.path}
                           getChildrenState={() => ({'source_rates': sourceRates})}>

        <Form.Field>
            <label>Source Rates</label>
            <Table singleLine>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Source Name</Table.HeaderCell>
                        <Table.HeaderCell>Rate</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {Object.keys(sourceRates).map((source, index) => {
                        console.log(source)
                        console.log(sourceRates)
                        return (<Table.Row key={index}>
                            <Table.Cell>{source}</Table.Cell>
                            <Table.Cell>
                                <Form.Input placeholder='Source Rate'
                                            type='number'
                                            value={sourceRates[source]}
                                            onChange={(e, {name, value}) => {
                                                setSourceRates({...sourceRates, [source]: value})
                                            }}
                                            required
                                />
                            </Table.Cell>
                        </Table.Row>)
                    })}
                </Table.Body>
            </Table>
        </Form.Field>

    </TransitionPage>
}