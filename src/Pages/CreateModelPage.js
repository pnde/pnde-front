import { Button, Checkbox, Container, Form, Header } from "semantic-ui-react";
import { useState } from "react";
import { RestManager } from "../AppStateManager";
import { useNavigate } from "react-router";

const ModelTypeOptions = [
    { key: 1, text: 'Stochastic Reward Petri Net', value: 10 },
    { key: 2, text: 'SB_SRN', value: 20 },
    { key: 3, text: 'MSRN', value: 30 },
    { key: 3, text: 'MPSRN', value: 40 },
]

const EngineOptions = [
    { key: 1, text: 'No Engine', value: 10 },
    { key: 2, text: 'SPNP', value: 20 },
]

const InitialState = {
    name: '',
    engine: 10,
    modelType: 10
}

export function CreateModelPage() {
    const [state, setState] = useState(InitialState)

    const handleChange = (e, { name, value }) => {
        setState({ ...state, [name]: value })
    }

    const navigate = useNavigate()

    return (
        <Container style={{ margin: 20 }}>
            <Header as="h3">Create New Model</Header>
            <Form onSubmit={() => RestManager.createNewModel(
                state.name,
                state.modelType,
                state.engine,
                (data) => {
                    console.log(data)
                    navigate('/main')
                }
            )}>
                <Form.Field>
                    <label>Model Name</label>
                    <Form.Input placeholder='Name' required name="name" onChange={handleChange} />
                </Form.Field>
                <Form.Field>
                    <Form.Select
                        required
                        fluid
                        label='Model Type'
                        options={ModelTypeOptions}
                        name='modelType'
                        onChange={handleChange}
                        value={state.modelType}
                    />
                </Form.Field>
                <Form.Field>
                    <Form.Select
                        required
                        fluid
                        label='Engine'
                        options={EngineOptions}
                        name='engine'
                        onChange={handleChange}
                        value={state.engine}
                    />
                </Form.Field>
                <Button fluid>Save</Button>
                <Button fluid
                    style={{ marginTop: 15 }}
                    onClick={(event) => {
                        event.preventDefault()
                        navigate('/model/select')
                    }}
                    primary>
                    Back
                </Button>
            </Form>

        </Container>
    )
}