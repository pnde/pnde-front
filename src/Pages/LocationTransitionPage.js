import TransitionPage from "./TransitionPage";
import { useState } from "react";
import { mergeLocationDeps } from "../Petri/LocationBased/Utils";
import { Form, Table } from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css'

const DepsOptions = [
    { key: 1, text: 'Not Selected', value: 'N' },
    { key: 2, text: 'Accept-base', value: 'A' },
    { key: 3, text: 'Deny-base', value: 'D' },
]

export function LocationTransitionPage(props) {
    const initialLocationDeps = mergeLocationDeps(props.elements.nodes, props.selectedNode.location_depends_type);
    console.log(initialLocationDeps)
    const [locationDeps, setLocationDeps] = useState(
        initialLocationDeps
    )
    return <TransitionPage setSelectedNode={props.setSelectedNode}
        selectedNode={props.selectedNode}
        triggerUpdate={props.triggerUpdate}
        elements={props.elements}
        path={props.path}
        getChildrenState={() => ({ 'location_depends_type': locationDeps })}>

        <Form.Field>
            <label>Location Deps</label>
            <Table singleLine>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Location Name</Table.HeaderCell>
                        <Table.HeaderCell>Dependency Type</Table.HeaderCell>
                        <Table.HeaderCell>Dependency Token Values (Separated by comma)</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {Object.keys(locationDeps).map((location, index) => {
                        return (<Table.Row key={index}>
                            <Table.Cell>{location}</Table.Cell>
                            <Table.Cell>
                                <Form.Select placeholder='Dependency'
                                    value={locationDeps[location].type}
                                    options={DepsOptions}
                                    onChange={(e, { name, value }) => {
                                        setLocationDeps({ ...locationDeps, [location]: { ...locationDeps[location], type: value } })
                                    }}
                                    required
                                />
                            </Table.Cell>
                            <Table.Cell>
                                <Form.Input placeholder='Dependency Token Values'
                                    type='text'
                                    value={locationDeps[location].set}
                                    onChange={(e, { name, value }) => {
                                        setLocationDeps({ ...locationDeps, [location]: { ...locationDeps[location], set: value } })
                                    }}
                                />
                            </Table.Cell>
                        </Table.Row>)
                    })}
                </Table.Body>
            </Table>
        </Form.Field>

    </TransitionPage>
}