export const getPageAddressType = (type) => {
    switch (type) {
        case 'place':
            return '/place'
        case 'immTransition':
            return '/transition/imm'
        case 'timedTransition':
            return '/transition/timed'
        case 'source':
            return '/sbpn/source'
        case 'sourceTimedTransition':
            return '/sbpn/transition/timed'
        case 'sourceImmTransition':
            return '/sbpn/transition/imm'
        case 'location':
            return '/msrn/location'
        case 'locationTimedTransition':
            return '/msrn/transition/timed'
        case 'locationImmTransition':
            return '/msrn/transition/imm'
        default:
            return '/error'
    }
}

export function getAvailableId(elements, type) {
    const ids = elements.filter(item => item.id[0] === type).map(item => item.id.slice(1)).map(item => parseInt(item))
    return ids.length ? Math.max(...ids) + 1 : 0
}