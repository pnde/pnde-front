import {useNavigate} from "react-router";
import {Button, Container, Form} from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css'
import {useState} from "react";
import {getUpdatedNodes} from "../Petri/utils";

export default function PlacePage(props) {
    const navigate = useNavigate();
    const [token, setToken] = useState(props.selectedNode.data.petri_token)
    const [name, setName] = useState(props.selectedNode.data.label)
    return (
        <Container style={{margin: 20}}>
            <Form onSubmit={() => {
                const state = {'data': {'petri_token': token, 'label': name}}
                const updatedNodes = getUpdatedNodes(props.selectedNode.id, state, props.elements.nodes);
                props.triggerUpdate('nodes', updatedNodes)
                navigate("/main")
            }}>
                <Form.Field>
                    <label>Place ID</label>
                    <input placeholder='Place ID'
                           value={props.selectedNode.id}
                           disabled
                    />
                </Form.Field>
                <Form.Field>
                    <label>Place Name</label>
                    <Form.Input placeholder='Place Name'
                                value={name}
                                onChange={(e, {name, value}) => {
                                    setName(value)
                                }}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Number of Tokens</label>
                    <Form.Input placeholder='Number of Tokens'
                                type='number'
                                min={0}
                                value={token}
                                onChange={(e, {name, value}) => {
                                    setToken(parseInt(value))
                                }}
                                required
                    />
                </Form.Field>
                <Form.Field>
                    <Button>Add Multiplicity Function</Button>
                </Form.Field>
                <Form.Button content="submit">
                    Save
                </Form.Button>
                <Form.Button onClick={() => navigate("/main")}>
                    Cancel
                </Form.Button>
                <Form.Button onClick={(event) => {
                    event.preventDefault()
                    setName(props.selectedNode.data.name)
                    setToken(props.selectedNode.data.petri_token)
                }}>
                    Reset
                </Form.Button>
            </Form>
        </Container>
    )
}