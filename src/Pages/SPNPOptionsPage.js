import {Button, Checkbox, Container, Icon, Input, Search, Select, Table} from "semantic-ui-react";
import React, {useState} from "react";
import _ from 'lodash'
import {defaultOptions} from "../Engines/spnp/Options";
import {useNavigate} from "react-router";
import {RestManager} from "../AppStateManager";

const source = _.times(5, () => ({
    title: "amin",
    description: "test",
    image: "",
    price: 200,
}))

const initialState = {
    loading: false,
    results: [],
    value: '',
}

function exampleReducer(state, action) {
    switch (action.type) {
        case 'CLEAN_QUERY':
            return initialState
        case 'START_SEARCH':
            return {...state, loading: true, value: action.query}
        case 'FINISH_SEARCH':
            return {...state, loading: false, results: action.results}
        case 'UPDATE_SELECTION':
            return {...state, value: action.selection}

        default:
            throw new Error()
    }
}

function SearchExampleStandard() {
    const [state, dispatch] = React.useReducer(exampleReducer, initialState)
    const {loading, results, value} = state

    const timeoutRef = React.useRef()
    const handleSearchChange = React.useCallback((e, data) => {
        clearTimeout(timeoutRef.current)
        dispatch({type: 'START_SEARCH', query: data.value})

        timeoutRef.current = setTimeout(() => {
            if (data.value.length === 0) {
                dispatch({type: 'CLEAN_QUERY'})
                return
            }

            const re = new RegExp(_.escapeRegExp(data.value), 'i')
            const isMatch = (result) => re.test(result.title)

            dispatch({
                type: 'FINISH_SEARCH',
                results: _.filter(Object.keys(defaultOptions).map((option) => {
                    return {
                        title: option,
                        onClick: () => {
                            const element = document.getElementById(option);
                            element.scrollIntoView()
                            const handler = () => {
                                element.style.visibility = (element.style.visibility == 'hidden' ? '' : 'hidden')
                            };
                            const intervalId = setInterval(handler, 500);
                            setTimeout(() => {
                                clearInterval(intervalId)
                                element.style.visibility = ''
                            }, 3000)
                        }
                    }
                }), isMatch),
            })
        }, 300)
    }, [])
    React.useEffect(() => {
        return () => {
            clearTimeout(timeoutRef.current)
        }
    }, [])

    return (

        <Search
            loading={loading}
            placeholder='Search...'
            onResultSelect={(e, data) =>
                dispatch({type: 'UPDATE_SELECTION', selection: data.result.title})
            }
            onSearchChange={handleSearchChange}
            results={results}
            value={value}
            onSelectionChange={(event, data) => console.log(event, data)}
        />

    )
}

export const SPNPOptionsPage = (props) => {
    const [options, setOptions] = useState(props.options)
    const navigate = useNavigate();

    const updateSelectValue = (event, {name, value}) => {
        const updatedOption = options[name]
        updatedOption['value'] = value
        updatedOption['checked'] = true
        console.log({...options, ...{[name]: updatedOption}})
        setOptions({...options, ...{[name]: updatedOption}})
        console.log(options)
    }

    const updateEnabled = (event, data) => {
        console.log(data)
        const updatedOption = options[data.name]
        console.log(updatedOption)
        updatedOption['checked'] = !updatedOption['checked']
        console.log(updatedOption)
        setOptions({...options, ...{[data.name]: updatedOption}})
        console.log(options)
    }

    const updateInputValue = (event, data) => {
        console.log(data)
        const updatedOption = options[data.name]
        console.log(updatedOption)
        updatedOption['value'] = data.value
        updatedOption['checked'] = true
        setOptions({...options, ...{[data.name]: updatedOption}})
        console.log(options)
    }

    return (
        <Container style={{margin: 20}}>
            <Table celled compact definition>
                <Table.Header fullWidth>
                    <Table.Row>
                        <Table.HeaderCell><SearchExampleStandard/></Table.HeaderCell>
                        <Table.HeaderCell colSpan='4'>
                            <Button
                                size='small'
                                onClick={() => {
                                    navigate("/main")
                                }}
                            >
                                Cancel
                            </Button>
                            <Button
                                floated='right'
                                icon
                                labelPosition='left'
                                primary
                                size='small'
                                onClick={() => {
                                    props.saveOptions(options)
                                    RestManager.saveModel(() => navigate("/main"), 'options', options)
                                }}
                            >
                                <Icon name='save'/>
                                Save
                            </Button>
                        </Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                        <Table.HeaderCell>Enable</Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Value</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {Object.keys(options).map((option) => {
                        const currentOption = options[option]
                        // console.log(option)
                        return (
                            <Table.Row key={option} id={option}>
                                <Table.Cell collapsing>
                                    <Checkbox slider onChange={updateEnabled}
                                              name={option} checked={currentOption.checked}/>
                                </Table.Cell>
                                <Table.Cell>{option}</Table.Cell>
                                <Table.Cell>{
                                    currentOption.type === 'select' ?
                                        <Select
                                            name={option}
                                            options={currentOption.values.map(item => ({
                                                key: item,
                                                value: item,
                                                text: item
                                            }))}
                                            onChange={updateSelectValue}
                                            value={currentOption.value}
                                            fluid/> :
                                        <Input
                                            name={option}
                                            type="number"
                                            value={currentOption.value}
                                            onChange={updateInputValue}
                                            fluid
                                        />
                                }</Table.Cell>
                            </Table.Row>
                        )
                    })}
                </Table.Body>
            </Table>
        </Container>
    )
}