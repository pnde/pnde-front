import {useState} from "react";

import Editor from "@monaco-editor/react";
import {Button, Menu} from "semantic-ui-react";
import {useNavigate} from "react-router";

const generateTransitionItems = (monaco, range, elements) => {
    let rate_suggestions = elements.nodes.filter(node => node.base_type === 't').map(node => ({
            label: `"Rate of Transition ${node.id} ${' ' + node.name ? node.name : ''}"`,
            kind: monaco.languages.CompletionItemKind.Function,
            insertText: `rate("${node.id}")`,
            range: range
        })
    )
    return [
        ...rate_suggestions,
    ]

}

const generateCompletionItems = (monaco, model, position, elements) => {
    let word = model.getWordUntilPosition(position);
    let range = {
        startLineNumber: position.lineNumber,
        endLineNumber: position.lineNumber,
        startColumn: word.startColumn,
        endColumn: word.endColumn
    };
    return {
        suggestions: generateTransitionItems(monaco, range, elements)
    }
}

const CodeEditorWindow = (props) => {
    const navigate = useNavigate();

    const handleEditorWillMount = (monaco) => {
        monaco.languages.registerCompletionItemProvider('c', {
            provideCompletionItems: (
                model,
                position,
                context,
                token
            ) => generateCompletionItems(monaco, model, position, props.elements)
        })
    }

    const [code, setCode] = useState(localStorage.getItem('code'))
    // const codeSetter = (value) => {
    //     setDevelopingCode(value)
    // }

    return (
        <div className="overlay rounded-md overflow-hidden w-full h-full shadow-4xl">
            <Menu style={{marginBottom: 0}}>
                <Menu.Item>
                    <Button
                        style={{marginRight: 15}}
                        primary
                        onClick={() => {
                            localStorage.setItem('code', code)
                            navigate(localStorage.getItem('resultPage'))
                        }}
                    >Save</Button>
                    <Button onClick={() => {
                        localStorage.removeItem('codeTarget')
                        navigate(localStorage.getItem('resultPage'))
                    }}>Cancel</Button>
                </Menu.Item>
            </Menu>
            <Editor
                height="100vh"
                width={`100%`}
                language="c"
                value={code}
                theme='vs-dark'
                beforeMount={handleEditorWillMount}
                defaultValue={code}
                onChange={setCode}
            />
        </div>
    );
};
export default CodeEditorWindow;