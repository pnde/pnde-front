import {useNavigate} from "react-router";
import {useState} from "react";
import {Container, Form} from "semantic-ui-react";
import {getUpdatedNode, getUpdatedNodes} from "../Petri/utils";

export function EdgePage(props) {
    const navigate = useNavigate();
    const initialState = {
        multiplicity: props.selectedEdge.multiplicity || 0,
        is_inhibitor: props.selectedEdge.is_inhibitor || false,
        has_multiplicity: props.selectedEdge.has_multiplicity || false,
        multiplicity_func: props.selectedEdge.multiplicity_func || `int multiplicty_func_${props.selectedEdge.id}() {}`
    }

    const [state, setState] = useState(initialState)
    console.log(state)
    console.log(props.selectedEdge.id)
    const handleChange = (e, {name, value}) => {
        setState({...state, [name]: value})
    }

    if (localStorage.getItem('codeTarget')) {
        setState({...state, [localStorage.getItem('codeTarget')]: localStorage.getItem('code')})
        localStorage.removeItem('code')
        localStorage.removeItem('codeTarget')
        localStorage.removeItem('resultPage')
    }

    return (
        <Container style={{margin: 20}}>
            <Form onSubmit={() => {
                console.log(props.elements.edges)
                const updatedEdges = getUpdatedNodes(
                    props.selectedEdge.id,
                    state,
                    props.elements.edges
                );
                console.log(updatedEdges)
                props.triggerUpdate('edges', updatedEdges)
                navigate("/main")
            }}>
                <Form.Field>
                    <label>Multiplicity</label>
                    <Form.Input placeholder='Multiplicity'
                                type='number'
                                name='multiplicity'
                                value={state.multiplicity}
                                onChange={handleChange}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Is Inhibitor Arc</label>
                    <Form.Checkbox
                        checked={state.is_inhibitor}
                        name='is_inhibitor'
                        label="is is_inhibitor"
                        onChange={
                            (event, data) => (
                                handleChange(event, {name: 'is_inhibitor', value: data.checked})
                            )
                        }
                    />
                </Form.Field>

                <Form.Group widths={"equal"}>
                    <Form.Checkbox
                        label="has multiplicity function"
                        checked={state.has_multiplicity}
                        name='has_multiplicity'
                        onChange={
                            (event, data) => (
                                handleChange(event, {name: 'has_multiplicity', value: data.checked})
                            )
                        }
                    />
                </Form.Group>
                <Form.Button
                    fluid
                    disabled={!state.has_multiplicity}
                    onClick={() => {
                        props.setSelectedEdge(getUpdatedNode(props.selectedEdge.id, state, props.elements.edges))
                        localStorage.setItem('codeTarget', 'multiplicity_func')
                        localStorage.setItem('code', state.multiplicity_func)
                        localStorage.setItem('resultPage', '/edge')
                        navigate("/code")
                    }}
                >
                    Edit Guard Function Code
                </Form.Button>

                <Form.Button primary fluid>
                    Save
                </Form.Button>
                <Form.Button
                    fluid
                    onClick={(event) => {
                        event.preventDefault()
                        setState(initialState)
                    }}>
                    Reset
                </Form.Button>
                <Form.Button
                    negative
                    fluid
                    onClick={() => navigate("/main")}>
                    Cancel
                </Form.Button>
            </Form>
        </Container>
    )
}