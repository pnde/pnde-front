import React from 'react'
import {Button, Container, Tab, Table} from 'semantic-ui-react'
import {useNavigate} from "react-router";

const ResultTable = (props) => (
    <Table celled singleLine>
        <Table.Header>
            {props.headers}
        </Table.Header>
        <Table.Body>
            {props.rows}
        </Table.Body>
    </Table>
)

export const SPNPResultsPage = () => {
    const navigate = useNavigate();
    const result = JSON.parse(localStorage.getItem('run_result'))
    console.log(result)

    const panes = [
        {
            menuItem: 'Results',
            render: () => <Tab.Pane attached={false}>
                <Container>
                    <h1>Net</h1>
                    <ResultTable
                        headers={
                            <Table.Row>
                                <Table.HeaderCell>Name</Table.HeaderCell>
                                <Table.HeaderCell>Value</Table.HeaderCell>
                            </Table.Row>
                        }
                        rows={(result.outputs.net || []).map(item => (
                            <Table.Row>
                                <Table.Cell>{item[0]}</Table.Cell>
                                <Table.Cell>{item[1]}</Table.Cell>
                            </Table.Row>
                        ))}
                    />
                    <h1>RG</h1>
                    <ResultTable
                        headers={
                            <Table.Row>
                                <Table.HeaderCell>Name</Table.HeaderCell>
                                <Table.HeaderCell>Value</Table.HeaderCell>
                            </Table.Row>
                        }
                        rows={(result.outputs.rg || []).map(item => (
                            <Table.Row>
                                <Table.Cell>{item[0]}</Table.Cell>
                                <Table.Cell>{item[1]}</Table.Cell>
                            </Table.Row>
                        ))}
                    />
                    <h1>Time</h1>
                    <p>{result.outputs.time}</p>
                    {(result.outputs.ctmc || []).length ? <h1>CTMC</h1> : <></>}
                    {(result.outputs.ctmc || []) ? <ResultTable
                        headers={
                            <></>
                        }
                        rows={(result.outputs.ctmc || []).map(item => (
                            <Table.Row>
                                <Table.Cell>{item[0]}</Table.Cell>
                                <Table.Cell>{item[1]}</Table.Cell>
                            </Table.Row>
                        ))}/> : <></>}
                    <h1>Places Average</h1>
                    {result.outputs.average ?
                        <ResultTable
                            headers={
                                <Table.Row>
                                    <Table.HeaderCell>{}</Table.HeaderCell>
                                    <Table.HeaderCell>{result.outputs.average.places[0][0]}</Table.HeaderCell>
                                    <Table.HeaderCell>{result.outputs.average.places[0][1]}</Table.HeaderCell>
                                    <Table.HeaderCell>{result.outputs.average.places[0][2]}</Table.HeaderCell>
                                </Table.Row>
                            }
                            rows={result.outputs.average.places.slice(1).map(item => (
                                <Table.Row>
                                    <Table.Cell>{item[0]}</Table.Cell>
                                    <Table.Cell>{item[1]}</Table.Cell>
                                    <Table.Cell>{item[2]}</Table.Cell>
                                    <Table.Cell>{item[3]}</Table.Cell>
                                </Table.Row>
                            ))}
                        /> : <></>}

                    <h1>Transition Average</h1>
                    {console.log(result.outputs.average)}
                    {result.outputs.average ?
                        <ResultTable
                            headers={
                                <Table.Row>
                                    <Table.HeaderCell>{}</Table.HeaderCell>
                                    <Table.HeaderCell>{result.outputs.average.transitions[0][0]}</Table.HeaderCell>
                                    <Table.HeaderCell>{result.outputs.average.transitions[0][1]}</Table.HeaderCell>
                                    <Table.HeaderCell>{result.outputs.average.transitions[0][2]}</Table.HeaderCell>
                                </Table.Row>
                            }
                            rows={result.outputs.average.transitions.slice(1).map(item => (
                                <Table.Row>
                                    <Table.Cell>{item[0]}</Table.Cell>
                                    <Table.Cell>{item[1]}</Table.Cell>
                                    <Table.Cell>{item[2]}</Table.Cell>
                                    <Table.Cell>{item[3]}</Table.Cell>
                                </Table.Row>
                            ))}
                        /> : <></>}
                </Container>
            </Tab.Pane>,
        },
        {
            menuItem: 'Logs Content',
            render: () => <Tab.Pane attached={false}>{result.logs.split('\n').map(str => <p>{str}</p>)}</Tab.Pane>,
        },
        {
            menuItem: 'Execution Output',
            render: () => <Tab.Pane attached={false}>{result.execution_output.split('\n').map(str =>
                <p>{str}</p>)}</Tab.Pane>,
        },
        {
            menuItem: 'Raw Output',
            render: () => <Tab.Pane attached={false}>
                {result.raw_output.map(item => <p>{item}</p>)}
            </Tab.Pane>
        }
    ]

    return (
        <Container style={{margin: 20}}>
            <Button
                primary
                onClick={() => navigate("/main")}
            >Back</Button>
            <Tab menu={{secondary: true, pointing: true}} panes={panes}/>
        </Container>
    )

}