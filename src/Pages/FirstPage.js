import {AppBar, Button, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup} from "@mui/material";
import {useNavigate} from "react-router";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import CssBaseline from "@mui/material/CssBaseline";
import {useState} from "react";
import {engineTypes, modeTypes} from "../Petri/Consts";
import {DefaultManager} from "../AppStateManager";

export default function FirstPage() {
    const navigate = useNavigate();
    const [type, setType] = useState(modeTypes[0].value);
    const [engine, setEngine] = useState(engineTypes[0].value);
    return (
        <Box sx={{flexGrow: 1}}>
            <CssBaseline/>
            <AppBar position="static">
                <Toolbar variant="dense">
                    <Typography variant="h6" color="inherit" component="div">
                        Petri Net Development Environment
                    </Typography>
                </Toolbar>
            </AppBar>
            <Box
                component="main"
                sx={{flexGrow: 1, p: 3, width: {sm: `calc(100% - 240px)`}}}
            >
                <Typography paragraph>
                    Welcome!
                    <br/>
                    Fill the Following Inputs and Press on the "Create New" Button to create a new model or click on the
                    "Open" Button.
                </Typography>
                <FormControl>
                    <FormLabel id="petri-radio-buttons-group-label">Model Type</FormLabel>
                    <RadioGroup
                        aria-labelledby="petri-radio-buttons-group-label"
                        defaultValue={modeTypes[0].value}
                        name="petri-radio-buttons-group"
                        onChange={(ev, value) => {
                            setType(value)
                            console.log(value)
                        }}
                    >
                        {modeTypes.map((o) => (
                            <FormControlLabel key={o.value} value={o.value} control={<Radio/>} label={o.name}/>
                        ))}
                    </RadioGroup>
                    <FormLabel id="engine-radio-buttons-group-label">Analyzer Engine</FormLabel>
                    <RadioGroup
                        aria-labelledby="engine-radio-buttons-group-label"
                        defaultValue={engineTypes[0].value}
                        name="engine-radio-buttons-group"
                        onChange={(ev, value) => {
                            setEngine(value)
                        }}
                    >
                        {engineTypes.map((o) => (
                            <FormControlLabel key={o.value} value={o.value} control={<Radio/>} label={o.name}/>
                        ))}
                    </RadioGroup>
                    <Box spacing={2} textAlign='center'>
                        <Button
                            variant="contained"
                            sx={{width: 400, p: 1, m: 1}}
                            onClick={() => {
                                DefaultManager.initApp()
                                DefaultManager.setEngine(engine)
                                DefaultManager.setModelType(type)
                                console.log(DefaultManager.isSBPNEnable())
                                navigate("/main")
                            }}
                        >
                            Create New
                        </Button>
                    </Box>
                    <Box spacing={2} textAlign='center'>
                        <Button variant="contained" sx={{width: 400, p: 1, m: 1}}>Open</Button>
                    </Box>
                </FormControl>
            </Box>
        </Box>
    )
}