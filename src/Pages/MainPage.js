import {styled, useTheme} from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import AdjustIcon from '@mui/icons-material/Adjust';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import NetEditor from "./NetEditor/NetEditor";
import useWindowDimensions from "../utils";
import PetriDrawerItems from "../Petri/DrawerItems";
import {useEffect, useState} from 'react';
import RedoIcon from '@mui/icons-material/Redo';
import UndoIcon from '@mui/icons-material/Undo';
import AddIcon from '@mui/icons-material/Add';
import SaveIcon from '@mui/icons-material/Save';
import {useNavigate} from "react-router";
import EngineDrawerItems from "../Engines/DrawerItems";
import {RestManager} from "../AppStateManager";
import {applyEdgeChanges, applyNodeChanges} from "reactflow";
import {getRelatedEdgesIds} from "../Petri/utils";
import {ArrowCircleUp} from "@mui/icons-material";

const drawerWidth = 400;

const openedMixin = (theme) => ({
    width: drawerWidth, transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp, duration: theme.transitions.duration.enteringScreen,
    }), overflowX: 'hidden',
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp, duration: theme.transitions.duration.leavingScreen,
    }), overflowX: 'hidden', width: `calc(${theme.spacing(7)} + 1px)`, [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(8)} + 1px)`,
    },
});

const DrawerHeader = styled('div')(({theme}) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1), // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({theme, open}) => ({
    zIndex: theme.zIndex.drawer + 1, transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp, duration: theme.transitions.duration.leavingScreen,
    }), ...(open && {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp, duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const Drawer = styled(MuiDrawer, {shouldForwardProp: (prop) => prop !== 'open'})(({theme, open}) => ({
    width: drawerWidth, flexShrink: 0, whiteSpace: 'nowrap', boxSizing: 'border-box', ...(open && {
        ...openedMixin(theme), '& .MuiDrawer-paper': openedMixin(theme),
    }), ...(!open && {
        ...closedMixin(theme), '& .MuiDrawer-paper': closedMixin(theme),
    }),
}),);

export default function MainPage(props) {
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const navigate = useNavigate();
    const [isSelectMode, setSelectMode] = useState(false)

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const {height, width} = useWindowDimensions();

    // const onConnect = useCallback((params) => setEdges((eds) => addEdge(params, eds)), [setEdges]);

    const removeNode = (node) => {
        const edgesIds = getRelatedEdgesIds(node.id, props.elements?.edges);
        if (edgesIds.length) {
            alert("remove edges to/from this node")
            return
        }
        const nodeChanges = applyNodeChanges([{id: node.id, type: 'remove'}], props.elements?.nodes);
        props.triggerUpdate('nodes', nodeChanges)
    }

    const removeEdge = (edge) => {
        props.triggerUpdate('edges', applyEdgeChanges([{id: edge.id, type: 'remove'}], props.elements?.edges))
    }

    return (<Box sx={{display: 'flex'}}>
        <CssBaseline/>
        <AppBar position="fixed" open={open}>
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    sx={{
                        marginRight: 5, ...(open && {display: 'none'}),
                    }}
                >
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" noWrap component="div">
                    {localStorage.getItem('model_name')}
                </Typography>
                <IconButton
                    color="inherit"
                    sx={{
                        marginLeft: 2,
                    }}
                    onClick={() => props.undo()}
                >
                    <UndoIcon/>
                </IconButton>
                <IconButton
                    color="inherit"
                    sx={{
                        marginLeft: 2,
                    }}
                    onClick={() => props.redo()}
                >
                    <RedoIcon/>
                </IconButton>
                <IconButton
                    color="inherit"
                    sx={{
                        marginLeft: 2,
                    }}
                    onClick={() => navigate("/")}
                >
                    <AddIcon/>
                </IconButton>
                <IconButton
                    color="inherit"
                    sx={{
                        marginLeft: 2,
                    }}
                    onClick={() => {
                        const flow = props.rfInstance.toObject();
                        localStorage.setItem('saved_node', flow)
                        RestManager.saveModel((result) => {
                            console.log(result)
                        }, 'model_data', flow)
                    }}
                >
                    <SaveIcon/>
                </IconButton>
                <IconButton
                    color={isSelectMode ? "inherit" : 'black'}
                    sx={{
                        marginLeft: 2,
                    }}
                    onClick={() => {
                        setSelectMode(true)
                        const cssTemplateString = `.place:before{visibility: visible} .transition:before{visibility: visible}`;
                        const styleTag = document.createElement("style");
                        styleTag.innerHTML = cssTemplateString;
                        document.head.insertAdjacentElement('beforeend', styleTag);
                    }}
                >
                    <AdjustIcon/>
                </IconButton>

                <IconButton
                    color={!isSelectMode ? "inherit" : 'black'}
                    sx={{
                        marginLeft: 2,
                    }}
                    onClick={() => {
                        setSelectMode(false)
                        const cssTemplateString = `.place:before{visibility: hidden} .transition:before{visibility: hidden}`;
                        const styleTag = document.createElement("style");
                        styleTag.innerHTML = cssTemplateString;
                        document.head.insertAdjacentElement('beforeend', styleTag);
                    }}
                >
                    <ArrowCircleUp/>
                </IconButton>


            </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
            <DrawerHeader>
                <IconButton onClick={handleDrawerClose}>
                    {theme.direction === 'rtl' ? <ChevronRightIcon/> : <ChevronLeftIcon/>}
                </IconButton>
            </DrawerHeader>
            <Divider/>
            <PetriDrawerItems open={open} elements={props.elements} setNodes={props.setElements}/>
            <EngineDrawerItems open={open} rfInstance={props.rfInstance}/>

        </Drawer>
        <Box component="main" sx={{flexGrow: 0, p: 0}}>
            <DrawerHeader></DrawerHeader>
            <div style={{width: width - 63, height: height - 64}}>
                <NetEditor nodes={props.elements?.nodes}
                           edges={props.elements?.edges}
                           onNodesChange={props.onNodesChange}
                           onEdgesChange={props.onEdgesChange}
                           onConnect={props.onConnect}
                           onInit={props.setRfInstance}
                           setSelectedNode={props.setSelectedNode}
                           triggerUpdate={props.triggerUpdate}
                           selectedEdge={props.selectedEdge}
                           setSelectedEdge={props.setSelectedEdge}
                           nodeRemover={removeNode}
                           removeEdge={removeEdge}
                />
            </div>
        </Box>
    </Box>);
}