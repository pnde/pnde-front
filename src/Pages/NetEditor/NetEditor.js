import ReactFlow, { Background, Controls, MarkerType, MiniMap, } from 'reactflow';
import 'reactflow/dist/style.css';
import { ImmTransition, Place, TimedTransition } from "../../Petri/Basic/Nodes";
import FloatingEdge from "./FloatingEdge";
import "../../Petri/Basic/Node.css"
import CustomConnectionLine from "./CustomConnectionLine";
import { useNavigate } from "react-router";
import { getPageAddressType } from "../utils";
import { Source, SourceImmTransition, SourceTimedTransition } from "../../Petri/SourceBased/Nodes";
import { Location, LocationImmTransition, LocationTimedTransition } from '../../Petri/LocationBased/Nodes';

const connectionLineStyle = {
    strokeWidth: 3,
    stroke: 'black',
};

const nodeTypes = {
    place: Place,
    immTransition: ImmTransition,
    timedTransition: TimedTransition,
    source: Source,
    location: Location,
    sourceTimedTransition: SourceTimedTransition,
    sourceImmTransition: SourceImmTransition,
    locationTimedTransition: LocationTimedTransition,
    locationImmTransition: LocationImmTransition,
};

const edgeTypes = {
    floating: FloatingEdge,
};

const defaultEdgeOptions = {
    style: { strokeWidth: 3, stroke: 'black' },
    type: 'floating',
    markerEnd: {
        type: MarkerType.ArrowClosed,
        color: 'black',
    },
};

function NetEditor(props) {
    const navigate = useNavigate();

    return (
        <ReactFlow
            nodes={props.nodes}
            edges={props.edges}
            onNodesChange={props.onNodesChange}
            onEdgesChange={props.onEdgesChange}
            onConnect={props.onConnect}
            onInit={props.onInit}
            nodeTypes={nodeTypes}
            edgeTypes={edgeTypes}
            defaultEdgeOptions={defaultEdgeOptions}
            connectionLineComponent={CustomConnectionLine}
            connectionLineStyle={connectionLineStyle}
            onNodeDoubleClick={(ev, node) => {
                props.setSelectedNode(node)
                navigate(getPageAddressType(node.type))
            }}

            onNodeContextMenu={(ev, node) => {
                ev.preventDefault()
                props.nodeRemover(node)
            }}
            onEdgeDoubleClick={(ev, edge) => {
                props.setSelectedEdge(edge)
                navigate('/edge')
            }}
            onEdgeContextMenu={(ev, edge) => {
                ev.preventDefault()
                props.removeEdge(edge)
            }}
        >
            <MiniMap />
            <Controls />
            <Background />
        </ReactFlow>
    );
}

export default NetEditor;