import {Button, Container, Header, Table} from "semantic-ui-react";
import {useNavigate} from "react-router";
import {useEffect, useState} from "react";
import {DefaultManager, RestManager} from "../AppStateManager";
import {userCode} from "../App";

export function SelectModelPage(props) {
    const navigate = useNavigate()
    const [items, setItems] = useState([])

    useEffect(() => {
        RestManager.getModels((result) => setItems(result))
    }, [])

    return (
        <Container style={{margin: 20}}>
            <Header as="h3">Select a Model</Header>
            <Table unstackable>
                <Table.Header fullWidth>
                    <Table.Row>
                        <Table.HeaderCell>Model Name</Table.HeaderCell>
                        <Table.HeaderCell>Model Type</Table.HeaderCell>
                        <Table.HeaderCell>Engine</Table.HeaderCell>
                        <Table.HeaderCell>Actions</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {items.map(item => (
                        <Table.Row key={item.id}>
                            <Table.Cell>{item.name}</Table.Cell>
                            <Table.Cell>{item.model_type_display}</Table.Cell>
                            <Table.Cell>{item.engine_display}</Table.Cell>
                            <Table.Cell>
                                <Button primary onClick={() => {
                                    localStorage.setItem('model_id', item.id)
                                    localStorage.setItem('model_name', item.name)
                                    localStorage.setItem('saved_node', item)
                                    localStorage.setItem("user_code", item.extra_code)
                                    localStorage.setItem("options", JSON.stringify(item.options))
                                    DefaultManager.setModelType(parseInt(item.model_type))
                                    DefaultManager.setEngine(parseInt(item.engine))
                                    props.setElements(item.model_data)
                                    navigate("/main")
                                }}>Load</Button>
                                <Button negative>Remove</Button>
                            </Table.Cell>
                        </Table.Row>)
                    )}
                </Table.Body>
            </Table>
            <Button fluid onClick={() => navigate('/model/new')}>Create New Model</Button>
        </Container>
    )
}