import {Button, Checkbox, Container, Dimmer, Form, Header, List, Loader, Segment} from "semantic-ui-react";
import {useState} from "react";
import {RestManager} from "../AppStateManager";
import {useNavigate} from "react-router";

export function LoginPage() {
    const [dimmer, setDimmerActive] = useState(false)
    const [rememberMe, setRememberMe] = useState(Boolean(localStorage.getItem('rememberMe')) || false)
    const [username, setUsername] = useState(localStorage.getItem('username') || '')
    const [password, setPassword] = useState(localStorage.getItem('password') || '')
    const navigate = useNavigate()
    localStorage.clear()

    return (
        <Segment>
            <Dimmer active={dimmer}>
                <Loader/>
            </Dimmer>

            <Container style={{margin: 20}}>
                <Header as="h3">PNDE</Header>
                <List>
                    <List.Item
                        as="p"
                        content="Welcome to the Petri Net Development Environment"
                    />
                    <List.Item
                        as="p"
                        content="Please Enter Your credential"
                    />
                </List>
                <Form onSubmit={() => {
                    setDimmerActive(true)
                    RestManager.login(username, password, rememberMe,
                        (result) => {
                            setDimmerActive(false)
                            if (result.token === undefined) {
                                alert("failed to login")
                                return
                            }
                            navigate('/model/select')
                        })
                }}>
                    <Form.Field>
                        <label>Username</label>
                        <input placeholder='Username'
                               value={username}
                               onChange={(event) => setUsername(event.target.value)}/>
                    </Form.Field>
                    <Form.Field>
                        <label>Password</label>
                        <input placeholder='Password'
                               type="password"
                               value={password}
                               onChange={(event) => setPassword(event.target.value)}/>
                    </Form.Field>
                    <Form.Field>
                        <Checkbox label='Remember Me' checked={rememberMe}
                                  onChange={(event, data) =>
                                      setRememberMe(data.checked)
                                  }
                        />
                    </Form.Field>
                    <Button type='submit' fluid>Submit</Button>
                </Form>
            </Container>
        </Segment>

    )
}