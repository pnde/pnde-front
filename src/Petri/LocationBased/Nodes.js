import { BasePetriNode, IsNodeTarget, Place } from "../Basic/Nodes";

export function Location({ id, data }) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = { zIndex: isTarget ? 3 : 1 };
    return <BasePetriNode mainColor='LightGreen'
        isTarget={isTarget}
        NodeStyleClassName="place"
        NodeBodyClassName="placeBody"
        targetHandleStyle={targetHandleStyle}
        id={id}
        extraLabel={<p>{`${id}${data.label ? " - " + data.label : ""}`}</p>}
        secondLabel={<p style={{ textAlign: "center" }}><br />{data.petri_token}</p>}
    />
}

export function LocationTimedTransition({ id, type }) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = { zIndex: isTarget ? 3 : 1 };
    return <BasePetriNode isTarget={isTarget}
        mainColor='MediumSeaGreen'
        NodeStyleClassName="transition"
        NodeBodyClassName="transitionBody"
        targetHandleStyle={targetHandleStyle}
        id={id}
        secondLabel={<p style={{ textAlign: "center" }}>{id}</p>}
    />
}

export function LocationImmTransition({ id, type }) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = { zIndex: isTarget ? 3 : 1 };
    return <BasePetriNode isTarget={isTarget}
        mainColor='DarkSeaGreen'
        NodeStyleClassName="transition"
        NodeBodyClassName="transitionBody"
        targetHandleStyle={targetHandleStyle}
        id={id}
        secondLabel={<p style={{ textAlign: "center" }}>{id}</p>}
    />
}