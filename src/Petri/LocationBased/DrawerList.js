import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import List from "@mui/material/List";
import { ReactComponent as AddLocation } from "../../Icons/AddLocation.svg";
import { ReactComponent as AddLImmTransition } from "../../Icons/AddLImmTransition.svg";
import { ReactComponent as AddLTimeTransition } from "../../Icons/AddLTimeTransition.svg";
import {
    createLocation,
    createLocationRelatedTimedTransition,
    createLocationRelatedImmediateTransition
} from "./EntetiesHandler";
import { SvgIcon } from "@mui/material";
import { getAvailableId } from "../../Pages/utils";

export default function LocationBasedPetriNetDrawerList(props) {
    const menuItems = [
        {
            title: 'Add Location',
            icon: <AddLocation />,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'P')
                props.setNodes(
                    {
                        nodes: [...(props.elements?.nodes ?? []), createLocation(id)],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
        {
            title: 'Add Time Location Related Transition',
            icon: <AddLTimeTransition />,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'T')
                props.setNodes(
                    {
                        nodes: [...(props.elements?.nodes ?? []), createLocationRelatedTimedTransition(id)],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
        {
            title: 'Add Immediate Location Related Transition',
            icon: <AddLImmTransition />,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'T')
                props.setNodes(
                    {
                        nodes: [...(props.elements?.nodes ?? []), createLocationRelatedImmediateTransition(id)],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
    ]
    return (<List>
        {menuItems.map((item) => (
            <ListItem key={item.title} disablePadding sx={{ display: 'block' }}>
                <ListItemButton onClick={() => item.action()}
                    sx={{
                        minHeight: 64,
                        justifyContent: props.open ? 'initial' : 'center',
                        px: 2.5,
                    }}
                >
                    <ListItemIcon
                        sx={{
                            minWidth: 0,
                            mr: props.open ? 3 : 'auto',
                            justifyContent: 'center',
                        }}
                    >
                        <SvgIcon>
                            {item.icon}
                        </SvgIcon>
                    </ListItemIcon>
                    <ListItemText primary={item.title} sx={{ opacity: props.open ? 1 : 0 }} />
                </ListItemButton>
            </ListItem>
        ))}
    </List>)
}