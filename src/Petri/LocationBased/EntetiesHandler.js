import { createPlace, createTransition } from "../Basic/EntetiesHandler";

export function createLocation(id) {
    return {
        ...createPlace({}, id),
        ...{
            "capacity": 0,
            "type": "location"
        }
    }
}

function createLocationRelatedTransition(nodeType, style, id) {
    return {
        location_depends_type: {},
        ...createTransition(nodeType, style, id)
    }
}

export function createLocationRelatedTimedTransition(id) {
    return createLocationRelatedTransition('locationTimedTransition', { backgroundColor: "CornflowerBlue" }, id)
}

export function createLocationRelatedImmediateTransition(id) {
    return createLocationRelatedTransition('locationImmTransition', { backgroundColor: 'royalblue' }, id)
}