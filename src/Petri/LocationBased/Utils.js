export const getLocations = (nodes) => {
    return nodes.filter((node) => {
        if (node.type === 'location')
            return node
    })
}

export const mergeLocationDeps = (nodes, locationSourceRates) => {
    const locations = getLocations(nodes)
    const result = {}
    locations.forEach((location) => {
        let dep = locationSourceRates[location.id]
        result[location.id] = dep ? dep : { type: "N", set: [] }
    })
    return result
}