export const SBPNModelTypeKey = 20;
export const MSRNModelTypeKey = 30;
export const MTSRNModelTypeKey = 40;
export const modeTypes = [
    {
        'name': 'Stochastic Petri Net',
        'value': 10,
    },
    {
        'name': 'Source-Based Petri Net',
        'value': SBPNModelTypeKey,
    },
    {
        'name': 'Location-Based Petri Net',
        'value': MSRNModelTypeKey,
    },
    {
        'name': 'Location-Source Based Petri Net',
        'value': MTSRNModelTypeKey,
    },
]

export const EngineTypeKey = 'engineType'
export const ModelTypeKey = 'modelType'

export const engineTypes = [
    {
        'name': 'SPNP',
        'value': 'spnp',
    },
    {
        'name': 'No Engine',
        'value': 'noEngine',
    },
]