export const getSources = (nodes) => {
    return nodes.filter((node) => {
        if (node.type === 'source')
            return node
    })
}

export const mergeSourceRates = (nodes, transitionSourceRates) => {
    const sources = getSources(nodes)
    console.log(transitionSourceRates)
    const result = {}
        sources.forEach((source) => {
        result[source.id] = transitionSourceRates[source.id] | 0
    })
    console.log(result)
    return result
}