import {createPlace, createTransition} from "../Basic/EntetiesHandler";

export function createSource(id) {
    return {
        ...createPlace({}, id),
        ...{
            "capacity": 0,
            "type": "source"
        }
    }
}

function createSourceRelatedTransition(nodeType, style, id) {
    return {
        source_rates: {},
        ...createTransition(nodeType, style, id)
    }
}

export function createSourceRelatedTimedTransition(id) {
    return createSourceRelatedTransition('sourceTimedTransition', {backgroundColor: "CornflowerBlue"}, id)
}

export function createSourceRelatedImmediateTransition(id) {
    return createSourceRelatedTransition('sourceImmTransition', {backgroundColor: 'royalblue'}, id)
}