import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import List from "@mui/material/List";
import {ReactComponent as AddSPlaceIcon} from "../../Icons/AddSPlace.svg";
import {ReactComponent as AddSImmTransition} from "../../Icons/AddSImmTransition.svg";
import {ReactComponent as AddSTimeTransition} from "../../Icons/AddSTimeTransition.svg";
import {
    createSource,
    createSourceRelatedImmediateTransition,
    createSourceRelatedTimedTransition
} from "./EntetiesHandler";
import {SvgIcon} from "@mui/material";
import {getAvailableId} from "../../Pages/utils";

export default function SourceBasedPetriNetDrawerList(props) {
    const menuItems = [
        {
            title: 'Add Source',
            icon: <AddSPlaceIcon/>,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'P')
                props.setNodes(
                    {
                        nodes: [...(props.elements?.nodes ?? []), createSource(id)],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
        {
            title: 'Add Time Source Related Transition',
            icon: <AddSTimeTransition/>,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'T')
                props.setNodes(
                    {
                        nodes: [...(props.elements?.nodes ?? []), createSourceRelatedTimedTransition(id)],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
        {
            title: 'Add Immediate Source Related Transition',
            icon: <AddSImmTransition/>,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'T')
                props.setNodes(
                    {
                        nodes: [...(props.elements?.nodes ?? []), createSourceRelatedImmediateTransition(id)],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
    ]
    return (<List>
        {menuItems.map((item) => (
            <ListItem key={item.title} disablePadding sx={{display: 'block'}}>
                <ListItemButton onClick={() => item.action()}
                                sx={{
                                    minHeight: 64,
                                    justifyContent: props.open ? 'initial' : 'center',
                                    px: 2.5,
                                }}
                >
                    <ListItemIcon
                        sx={{
                            minWidth: 0,
                            mr: props.open ? 3 : 'auto',
                            justifyContent: 'center',
                        }}
                    >
                        <SvgIcon>
                            {item.icon}
                        </SvgIcon>
                    </ListItemIcon>
                    <ListItemText primary={item.title} sx={{opacity: props.open ? 1 : 0}}/>
                </ListItemButton>
            </ListItem>
        ))}
    </List>)
}