import {BasePetriNode, IsNodeTarget, Place} from "../Basic/Nodes";

export function Source({id, data}) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = {zIndex: isTarget ? 3 : 1};
    return <BasePetriNode mainColor='LightSkyBlue'
                          isTarget={isTarget}
                          NodeStyleClassName="place"
                          NodeBodyClassName="placeBody"
                          targetHandleStyle={targetHandleStyle}
                          id={id}
                          extraLabel={<p>{`${id}${data.label ? " - " + data.label : ""}`}</p>}
                          secondLabel={<p style={{textAlign: "center"}}><br/>{data.petri_token}</p>}
    />
}

export function SourceTimedTransition({id, type}) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = {zIndex: isTarget ? 3 : 1};
    return <BasePetriNode isTarget={isTarget}
                          mainColor='CornflowerBlue'
                          NodeStyleClassName="transition"
                          NodeBodyClassName="transitionBody"
                          targetHandleStyle={targetHandleStyle}
                          id={id}
                          secondLabel={<p style={{textAlign: "center"}}>{id}</p>}
    />
}

export function SourceImmTransition({id, type}) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = {zIndex: isTarget ? 3 : 1};
    return <BasePetriNode isTarget={isTarget}
                          mainColor='steelblue'
                          NodeStyleClassName="transition"
                          NodeBodyClassName="transitionBody"
                          targetHandleStyle={targetHandleStyle}
                          id={id}
                          secondLabel={<p style={{textAlign: "center"}}>{id}</p>}
    />
}