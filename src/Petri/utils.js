export const getUpdatedNodes = (toUpdatedNodeId, updatedProps, nodes) => nodes.map((node) => {
    if (node.id === toUpdatedNodeId) {
        Object.entries(updatedProps).forEach(([key, value]) => {
            node[key] = value
            console.log(node)
        });
    }
    return node
})

export const getUpdatedNode = (toUpdatedNodeId, updatedProps, nodes) => {
    let UpdatedNode = null
    nodes.map((node) => {
        if (node.id === toUpdatedNodeId) {
            Object.entries(updatedProps).forEach(([key, value]) => {
                node[key] = value
            });
            UpdatedNode = node
        }
    })
    return UpdatedNode
}

export function getRelatedEdgesIds(nodeID, edges) {
    console.log(edges)
    return edges.filter(edge => edge.source === nodeID || edge.target === nodeID).map(edge => edge.id)
}