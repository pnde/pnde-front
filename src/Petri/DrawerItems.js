import Divider from "@mui/material/Divider";
import DefaultPetriNetDrawerList from "./Basic/DrawerList";
import SourceBasedPetriNetDrawerList from "./SourceBased/DrawerList";
import { DefaultManager } from "../AppStateManager";
import LocationBasedPetriNetDrawerList from "./LocationBased/DrawerList"
export default function PetriDrawerItems(props) {
    return (
        <div>
            <DefaultPetriNetDrawerList open={props.open} elements={props.elements} setNodes={props.setNodes} />
            <Divider />
            {DefaultManager.isSBPNEnable() || DefaultManager.isMTSRNEnable() ?
                <SourceBasedPetriNetDrawerList open={props.open}
                    elements={props.elements}
                    setNodes={props.setNodes} /> : <></>}
            {DefaultManager.isSBPNEnable() || DefaultManager.isMTSRNEnable() ? <Divider /> : <></>}
            {DefaultManager.isMSRNEnable() || DefaultManager.isMTSRNEnable() ?
                <LocationBasedPetriNetDrawerList open={props.open}
                    elements={props.elements}
                    setNodes={props.setNodes} /> : <></>}
            {DefaultManager.isSBPNEnable() || DefaultManager.isMTSRNEnable() ? <Divider /> : <></>}
        </div>
    )
}