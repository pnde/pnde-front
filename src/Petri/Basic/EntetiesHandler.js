export function createPlace(style, id) {
    return {
        name: '',
        type: 'place',
        t: 'nodes',
        id: `P${id}`,
        position: {x: 0, y: 0},
        style: style,
        data: {
            label: "",
            petri_token: 0,
        },
        petri_node_type: "place",
    }
}

export function createTransition(nodeType, style, id) {
    return {
        type: nodeType,
        t: 'nodes',
        id: `T${id}`,
        position: {x: 0, y: 0},
        data: {
            label: "",
        },
        style: {width: 32, height: 64, ...style},
        petri_node_type: nodeType,
        rate: 0.0,
        name: '',
        rate_mode: 'use_hard_code',
        rate_function_code: `// rate function for ${id}\n// dont remove this function!\ndouble rate_of_T${id}() {}`,
        priority: 0,
        policy: 'PRD',
        guard_mode: 'no_guard',
        guard_function: `// guard function for ${id}\n// dont remove this function!\ndouble guard_of_T${id}() {}`,
        place_name: ''
    }
}

export function createTimedTransition(id) {
    return createTransition('timedTransition', {}, id)
}

export function createImmediateTransition(id) {
    return createTransition('immTransition', {backgroundColor: 'grey'}, id)
}