import {Handle, Position, useStore} from 'reactflow';

const connectionNodeIdSelector = (state) => state.connectionNodeId;

export function BasePetriNode(props) {
    return (
        <div className={props.NodeStyleClassName}>
            <div
                className={props.NodeBodyClassName}
                style={{
                    borderStyle: props.isTarget ? 'dashed' : 'solid',
                    backgroundColor: props.isTarget ? '#ffcce3' : props.mainColor,
                }}
            >
                {props.extraLabel}
                <Handle
                    className="targetHandle"
                    style={{zIndex: 2}}
                    position={Position.Right}
                    type="source"
                />
                <Handle
                    className="targetHandle"
                    style={props.targetHandleStyle}
                    position={Position.Left}
                    type="target"
                />
                {props.secondLabel}
            </div>
        </div>
    )
}

export function IsNodeTarget(nodeId) {
    const connectionNodeId = useStore(connectionNodeIdSelector);
    return (
        connectionNodeId && connectionNodeId[0] !== nodeId[0]
    )
}

export function Place({id, data}) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = {zIndex: isTarget ? 3 : 1};
    return <BasePetriNode mainColor='white'
                          isTarget={isTarget}
                          NodeStyleClassName="place"
                          NodeBodyClassName="placeBody"
                          targetHandleStyle={targetHandleStyle}
                          id={id}
                          extraLabel={<p>{data.label ? data.label : id}</p>}
                          secondLabel={<p style={{textAlign: "center"}}><br/>{data.petri_token}</p>}
    />
}

export function TimedTransition({id, data}) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = {zIndex: isTarget ? 3 : 1};
    return <BasePetriNode isTarget={isTarget}
                          mainColor='white'
                          NodeStyleClassName="transition"
                          NodeBodyClassName="transitionBody"
                          targetHandleStyle={targetHandleStyle}
                          id={id}
                          secondLabel={<p>{data.label ? data.label : id}</p>}
    />
}

export function ImmTransition({id, data}) {
    const isTarget = IsNodeTarget(id)
    const targetHandleStyle = {zIndex: isTarget ? 3 : 1};
    return <BasePetriNode isTarget={isTarget}
                          mainColor='grey'
                          NodeStyleClassName="transition"
                          NodeBodyClassName="transitionBody"
                          targetHandleStyle={targetHandleStyle}
                          id={id}
                          secondLabel={<p>{data.label ? data.label : id}</p>}
    />
}