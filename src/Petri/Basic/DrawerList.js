import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import List from "@mui/material/List";
import {ReactComponent as AddPlaceIcon} from "../../Icons/AddPlace.svg";
import {ReactComponent as AddImmTransitionIcon} from "../../Icons/AddImmTransition.svg";
import {ReactComponent as AddTimeTransitionIcon} from "../../Icons/AddTimeTransition.svg";
import {SvgIcon} from "@mui/material";
import {createImmediateTransition, createPlace, createTimedTransition} from "./EntetiesHandler";
import {getAvailableId} from "../../Pages/utils";

export default function DefaultPetriNetDrawerList(props) {
    const menuItems = [
        {
            title: 'Add Place',
            icon: <AddPlaceIcon/>,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'P')
                console.log(id)
                props.setNodes(
                    {
                        nodes: [...(props.elements?.nodes ?? []), createPlace({}, id)],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
        {
            title: 'Add Timed Transition',
            icon: <AddTimeTransitionIcon/>,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'T')
                props.setNodes(
                    {
                        nodes: [createTimedTransition(id), ...(props.elements?.nodes ?? [])],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
        {
            title: 'Add Immediate Transition',
            icon: <AddImmTransitionIcon/>,
            action: () => {
                const id = getAvailableId(props.elements?.nodes ?? [], 'T')
                props.setNodes(
                    {
                        nodes: [createImmediateTransition(id), ...(props.elements?.nodes ?? [])],
                        edges: props.elements?.edges ?? []
                    }
                )
            }
        },
    ]
    return <List>
        {menuItems.map((item) => (
            <ListItem key={item.title} disablePadding sx={{display: 'block'}}>
                <ListItemButton onClick={() => item.action()}
                                sx={{
                                    minHeight: 64,
                                    justifyContent: props.open ? 'initial' : 'center',
                                    px: 2.5,
                                }}
                >
                    <ListItemIcon
                        sx={{
                            minWidth: 0,
                            mr: props.open ? 3 : 'auto',
                            justifyContent: 'center',
                        }}
                    >
                        <SvgIcon>
                            {item.icon}
                        </SvgIcon>
                    </ListItemIcon>
                    <ListItemText primary={item.title} sx={{opacity: props.open ? 1 : 0}}/>
                </ListItemButton>
            </ListItem>
        ))}
    </List>
}