export const defaultOptions = {
    IOP_PR_RSET: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO', 'VAL_TAN'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_RGRAPH: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_MARK_ORDER: {
        type: 'select',
        values: ['VAL_CANONIC', 'VAL_LEXICAL', 'VAL_MATRIX'],
        value: 'VAL_CANONIC',
        default: 'VAL_CANONIC',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_MERG_MARK: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_YES',
        default: 'VAL_YES',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_FULL_MARK: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_USENAME: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_MC: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_DERMC: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_MC_ORDER: {
        type: 'select',
        values: ['VAL_FROMTO', 'VAL_TOFROM'],
        value: 'VAL_FROMTO',
        default: 'VAL_FROMTO',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_PROB: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_PROBDTMC: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_PR_DOT: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_MC: {
        type: 'select',
        values: ['VAL_CTMC', 'VAL_DTMC'],
        value: 'VAL_CTMC',
        default: 'VAL_CTMC',
        checked: false,
        function: 'iopt'
    },
    IOP_SSMETHOD: {
        type: 'select',
        values: ['VAL_SSSOR', 'VAL_GASEI', 'VAL_POWER'],
        value: 'VAL_SSSOR',
        default: 'VAL_SSSOR',
        checked: false,
        function: 'iopt'
    },
    IOP_SSDETECT: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_YES',
        default: 'VAL_YES',
        checked: false,
        function: 'iopt'
    },
    FOP_SSPRES: {
        type: 'double',
        value: 0.25,
        default: 0.25,
        checked: false,
        function: 'fopt'
    },
    IOP_TSMETHOD: {
        type: 'select',
        values: ['VAL_TSUNIF', 'VAL_FOXUNIF'],
        value: 'VAL_FOXUNIF',
        default: 'VAL_FOXUNIF',
        checked: false,
        function: 'iopt'
    },
    IOP_CUMULATIVE: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_YES',
        default: 'VAL_YES',
        checked: false,
        function: 'iopt'
    },
    IOP_SENSITIVITY: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_ITERATIONS: {
        type: 'int',
        value: 2000,
        default: 2000,
        checked: false,
        function: 'iopt'
    },
    FOP_PRECISION: {
        type: 'double',
        value: 0.000001,
        default: 0.000001,
        checked: false,
        function: 'fopt'
    },
    IOP_SIMULATION: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_SIM_RUNS: {
        type: 'int',
        value: 0,
        default: 0,
        checked: false,
        function: 'iopt'
    },
    IOP_SIM_RUNMETHOD: {
        type: 'select',
        values: ['VAL_REPL', 'VAL_BATCH', 'VAL_RESTART', 'VAL_SPLIT', 'VAL_IS', 'VAL_THIN', 'VAL_ISTHIN', 'VAL_REG', 'VAL_ISREG'],
        value: 'VAL_REPL',
        default: 'VAL_REPL',
        checked: false,
        function: 'iopt'
    },
    IOP_SIM_SEED: {
        type: 'int',
        value: 52836,
        default: 52836,
        checked: false,
        function: 'iopt'
    },
    IOP_SIM_CUMULATIVE: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_YES',
        default: 'VAL_YES',
        checked: false,
        function: 'iopt'
    },
    IOP_SIM_STD_REPORT: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_YES',
        default: 'VAL_YES',
        checked: false,
        function: 'iopt'
    },
    IOP_SPLIT_LEVEL_DOWN: {
        type: 'double',
        value: 60,
        default: 60,
        checked: false,
        function: 'iopt'
    },
    IOP_SPLIT_PRESIM: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_YES',
        default: 'VAL_YES',
        checked: false,
        function: 'iopt'
    },
    IOP_SPLIT_NUMBER: {
        type: 'double',
        value: 6,
        default: 6,
        checked: false,
        function: 'iopt'
    },
    IOP_SPLIT_RESTART_FINISH: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_SPLIT_PRESIM_RUNS: {
        type: 'double',
        value: 10,
        default: 10,
        checked: false,
        function: 'iopt'
    },
    FOP_SIM_LENGTH: {
        type: 'double',
        value: 0.0,
        default: 0.0,
        checked: false,
        function: 'fopt'
    },
    FOP_SIM_CONFIDENCE: {
        type: 'double',
        value: 0.95,
        default: 0.95,
        checked: false,
        function: 'iopt'
    },
    FOP_SIM_ERROR: {
        type: 'double',
        value: 0.1,
        default: 0.1,
        checked: false,
        function: 'iopt'
    },

    IOP_ELIMINATION: {
        type: 'select',
        values: ['VAL_REDONTHEFLY', 'VAL_REDAFTERRG', 'VAL_REDNEVER'],
        value: 'VAL_REDONTHEFLY',
        default: 'VAL_REDONTHEFLY',
        checked: false,
        function: 'iopt'
    },
    IOP_OK_ABSMARK: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_OK_VANLOOP: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    IOP_OK_TRANS_MO: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_YES',
        default: 'VAL_YES',
        checked: false,
        function: 'iopt'
    },
    IOP_OK_VAN_MO: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_YES',
        default: 'VAL_YES',
        checked: false,
        function: 'iopt'
    },
    FOP_ABS_RET_M0: {
        type: 'double',
        value: 0.0,
        default: 0.0,
        checked: false,
        function: 'fopt'
    },
    IOP_DEBUG: {
        type: 'select',
        values: ['VAL_YES', 'VAL_NO'],
        value: 'VAL_NO',
        default: 'VAL_NO',
        checked: false,
        function: 'iopt'
    },
    FOP_FLUID_EPSILON: {
        type: 'double',
        value: 0.000001,
        default: 0.000001,
        checked: false,
        function: 'fopt'
    },
    FOP_TIME_EPSILON: {
        type: 'double',
        value: 0.000001,
        default: 0.000001,
        checked: false,
        function: 'fopt'
    },
}