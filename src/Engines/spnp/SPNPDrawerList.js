import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import ListItemIcon from "@mui/material/ListItemIcon";
import DataObjectIcon from '@mui/icons-material/DataObject';
import CodeIcon from '@mui/icons-material/Code';
import TuneIcon from '@mui/icons-material/Tune';
import DirectionsRunIcon from '@mui/icons-material/DirectionsRun';
import {useNavigate} from "react-router";
import {userCode} from "../../App";
import {RestManager} from "../../AppStateManager";

export default function SPNPDrawerList(props) {
    const navigate = useNavigate();
    const menuItems = [
        {
            title: 'Add User Code',
            icon: <DataObjectIcon/>,
            action: () => {
                localStorage.setItem('codeTarget', 'userCode')
                localStorage.setItem('code', localStorage.getItem("user_code"))
                localStorage.setItem('resultPage', "/main")
                navigate("/code")
            }
        },
        {
            title: 'Set Model Options',
            icon: <TuneIcon/>,
            action: () => {
                navigate("/spnp/options")
            }
        },
        {
            title: 'View Code Result',
            icon: <CodeIcon/>,
            action: () => {
                const flow = props.rfInstance.toObject();
                localStorage.setItem('saved_node', flow)
                RestManager.saveModel((result) => {
                    RestManager.getSPNPCode((code) => {
                        localStorage.setItem('codeTarget', 'spnpResultCode')
                        localStorage.setItem('code', code)
                        localStorage.setItem('resultPage', "/main")
                        navigate("/code")
                    })
                }, 'model_data', flow)
            }
        },
        {
            title: 'Run Model',
            icon: <DirectionsRunIcon/>,
            action: () => {
                const flow = props.rfInstance.toObject();
                localStorage.setItem('saved_node', flow)
                RestManager.saveModel((result) => {
                    RestManager.runSPNP((runResult) => {
                        localStorage.setItem('run_result', JSON.stringify(runResult))
                        navigate("/spnp/run")
                    })
                }, 'model_data', flow)
            }
        },
    ]
    return <List>
        {menuItems.map((item) => (
            <ListItem key={item.title} disablePadding sx={{display: 'block'}}>
                <ListItemButton onClick={() => item.action()}
                                sx={{
                                    minHeight: 64,
                                    justifyContent: props.open ? 'initial' : 'center',
                                    px: 2.5,
                                }}
                >
                    <ListItemIcon
                        sx={{
                            minWidth: 0,
                            mr: props.open ? 3 : 'auto',
                            justifyContent: 'center',
                        }}
                    >
                        {item.icon}
                    </ListItemIcon>
                    <ListItemText primary={item.title} sx={{opacity: props.open ? 1 : 0}}/>
                </ListItemButton>
            </ListItem>
        ))}
    </List>
}