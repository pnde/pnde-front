import SPNPDrawerList from "./spnp/SPNPDrawerList";

export default function EngineDrawerItems(props) {
    return (
        <div>
            <SPNPDrawerList open={props.open} rfInstance={props.rfInstance}/>
        </div>
    )
}